import os.path
from os import path
from flask import Flask, render_template, request

app = Flask(__name__)

@app.route("/<name>")
def hello(name):
    URL = request.__dict__
    URL = str(URL['environ']['REQUEST_URI'])
    if ".." in str(URL):
        return render_template('403.html', title = '403'), 403
    elif "//" in str(URL):
        return render_template('403.html', title = '403'), 403
    elif "~" in str(URL):
        return render_template('403.html', title = '403'), 403
    if os.path.exists("./templates/"+name):
        return render_template(str(name))
    else:
        return render_template('404.html', title = '404'), 404

@app.errorhandler(403)
def error_403(error):
    #to catch any potential error403 condition that I may not know of
    #other than what was specified in class
    return render_template('403.html', title = '403'), 403

@app.errorhandler(404)
def error_404(error):
    #to catch anything that could potentially get by that, I think
    #all the cases are good though. Better safe than sorry.
    return render_template('404.html', title = '404'), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
